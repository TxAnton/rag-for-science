FROM python:3.9

WORKDIR /app
 
COPY pyproject.toml /app/pyproject.toml
COPY poetry.lock /app/poetry.lock
COPY README.md /app/README.md
COPY main.py /app/main.py

RUN pip3 install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --with dev 